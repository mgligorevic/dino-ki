"""
This class implements a frame grabber.

When run, it yields a constant stream of stills from the screen.

Example:
    fg = FrameGrabber()
    bounding_box = (100,100,640,480)
    for _ in range(10):
        f = fg.get_frame(bounding_box)
        do_something_with(f)
"""

# ImageGrab from PIL could be used too, but its less performant!
import mss
import numpy

class FrameGrabber(object):

    _sct = None

    def __init__(self):
        self._sct = mss.mss()

    def get_frame(self, bounding_box):
        img = self._sct.grab(bounding_box)
        img_array = numpy.asarray(img)
        return img_array