# press 'q' to quit

import cv2
from frame_grabber import FrameGrabber

fg = FrameGrabber()

for _ in range(100):
    bounding_box = (100,100,640,480)
    img = fg.get_frame(bounding_box)
    cv2.imshow("test", img)
    if cv2.waitKey(25) & 0xFF == ord("q"):
        cv2.destroyAllWindows()
        break